# About this helmfile

Had to add this to the acquis.yaml that was generated after the first run:
```
---
source: syslog
listen_addr: 127.0.0.1
listen_port: 4242
labels:
 type: syslog
```
